# Changelog for gloss-export

## 0.1.0.4
Version bump  
reintroduction of comment regarding image size limit

contributers: Samuel Gélineau

## 0.1.0.3
Version bump  
removal of misleading comment in documentation

contributers: Samuel Gélineau

## 0.1.0.2
Error handling (throws error now, formerly stdout)  
Unified approach for RGBA,RGBA8  
Image is flipped by OpenGL now  
easier to compile on macOS, still works on linux

contributers: Samuel Gélineau


## 0.1.0.1
window no longer displayed when exporting  
added testsuite

contributers: Samuel Gélineau

## 0.1.0.0
Initial version
