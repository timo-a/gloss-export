-- | Main module for exporting gloss pictures to image files, gif animations and juicy-pixels image datatypes.
--   While no screen is displayed during the export process, the canvas is still limited to the screen's
--   resolution.

module Graphics.Gloss.Export
    (
    -- * For tinkering yourself
      withGlossState
    , withImage
    , withImages
    , exportPictureToFormat
    , exportPicturesToFormat
    
    -- * Writing to PNG
    , exportPictureToPNG
    , exportPicturesToPNG

    -- * Writing to Bitmap
    , exportPictureToBitmap
    , exportPicturesToBitmap

    -- * Writing to Tga
    , exportPictureToTga
    , exportPicturesToTga

    -- * Writing to Tiff
    , exportPictureToTiff
    , exportPicturesToTiff    

    -- * Writing to Gif
    , exportPicturesToGif
    , GifDelay(..)
    , GifLooping(..)
    ) where

import Graphics.Gloss.Export.Image
import Graphics.Gloss.Export.PNG
import Graphics.Gloss.Export.Bitmap
import Graphics.Gloss.Export.Tga
import Graphics.Gloss.Export.Tiff
import Graphics.Gloss.Export.Gif
