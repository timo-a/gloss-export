module Graphics.Gloss.Export.Tiff
    ( exportPictureToTiff
    , exportPicturesToTiff
    ) where

import Codec.Picture.Tiff (writeTiff)
import qualified Graphics.Gloss.Rendering as Gloss

import Graphics.Gloss.Export.Image

-- | Save a gloss Picture as Tiff
exportPictureToTiff :: Size -- ^ width, height in pixels 
                   -> Gloss.Color -- ^ background color
                   -> FilePath -> Gloss.Picture -> IO ()
exportPictureToTiff  = exportPictureToFormat writeTiff

-- | Save a gloss animation as Tiff
exportPicturesToTiff :: Size        -- ^ width, height in pixels 
                    -> Gloss.Color -- ^ background color
                    -> FilePath
                    -> Animation   -- ^ function that maps from point in time to Picture. analog to Gloss.Animation
                    -> [Float]     -- ^ list of points in time at which to evaluate the animation
                    -> IO ()
exportPicturesToTiff = exportPicturesToFormat writeTiff
